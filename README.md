# Kibana
## Prérequis
Pour utiliser le projet Kibana et récupérer les logs de votre projet, vous devez vous assurer que les containers du projet soient bien configurés et lancés avant de lancer ce pack.
    
## Démarrer l'environnement docker ELK
    cd kibana
    sudo make add_host
    make start
## Accéder à kibana
Les containers tournent en tâche de fond.

Attendre une ou deux minutes que le container Filebeat récupère les logs du projet, puis aller sur l'url :

http://kibana.docker:5601
