DOCKER = docker
DOCKER_COMPOSE          = docker-compose
AWK                     := $(shell command -v awk 2> /dev/null)
IP_ADDRESS              = 11.0.0.14
URL                     = kibana.docker

help:
ifndef AWK
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
else
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
endif

##
## Docker
#################################

build:
	$(DOCKER_COMPOSE) build --no-cache
.PHONY: build

restart:
	make down
	make up

up:
	$(DOCKER_COMPOSE) up -d --remove-orphans

down: ## Down environment
down:
	$(DOCKER_COMPOSE) down --remove-orphans --volumes

logs: ## View output from containers
logs:
	$(DOCKER_COMPOSE) logs -f
ps:
	$(DOCKER_COMPOSE) ps

filebeat_bash: ## access to filebeat container
	$(DOCKER) exec -it kibana_filebeat_1 bash

##
## Project
#################################

start: ## Start the project
start: build up

##
## Config
#################################

add_host: ## Add host in /etc/hosts (Need be root)
add_host:
	sed --in-place=.bak -n '/$(IP_ADDRESS)/!p' /etc/hosts && echo "$(IP_ADDRESS) $(URL)" >> /etc/hosts
